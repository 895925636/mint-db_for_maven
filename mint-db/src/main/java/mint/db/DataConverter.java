package mint.db;

public interface DataConverter<T> {
	/**
	 * 将bean的field值序列化成数据库column的值
	 * @param fieldValue
	 * @return
	 */
	public Object fieldToColumn(Object fieldValue);
	
	/**
	 * 决定如何将数据库的数据转换成bean的field
	 * @param databaseValue
	 * @param propType
	 * @param columnType
	 * @return
	 */
	public Object ColumnToField(String databaseValue, Class<?> propType, String columnType);
}
